-module(rsc_a_aid).
 
-compile(export_all).
 
-include_lib("webmachine/include/webmachine.hrl").
 
init(Config) ->
    {{trace, "priv/log"}, Config}. %% debugging
    % {ok, undefined}.

allowed_methods(RD, Ctx) ->
    {['GET', 'HEAD'], RD, Ctx}.
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GET
resource_exists(RD, Ctx) ->
    Aid = wrq:path_info(aid, RD),
    A = a:read(Aid),
    case A of
        {error, _} ->
            Result = false,
            Ctx_Next = Ctx;
        _ ->
            Result = true,
            % push the Answer
            Ctx_Next = [A | Ctx]
    end,
    {Result, RD, Ctx_Next}.

content_types_provided(RD, Ctx) ->
    {[{"application/json", to_json} ], RD, Ctx}.
 
to_json(RD, Ctx) ->
    % pop the Answer
    [A | Ctx_Next] = Ctx,
    Resp = a:to_json(A),
    {Resp, RD, Ctx_Next}.
