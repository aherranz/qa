-module(rsc_q_qid).
 
-compile(export_all).
 
-include_lib("webmachine/include/webmachine.hrl").
 
init(Config) ->
    {{trace, "priv/log"}, Config}. %% debugging
    % {ok, undefined}.

allowed_methods(RD, Ctx) ->
    {['GET', 'HEAD', 'DELETE'], RD, Ctx}.
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GET
resource_exists(RD, Ctx) ->
    Qid = wrq:path_info(qid, RD),
    Q = q:read(Qid),
    case Q of
        {error, _} ->
            Result = false,
            Ctx_Next = Ctx;
        _ ->
            Result = true,
            % push the Question 
            Ctx_Next = [Q | Ctx]
    end,
    {Result, RD, Ctx_Next}.

delete_resource(RD, Ctx) ->
  Qid = wrq:path_info(qid, RD),
  Q = q:delete(Qid),
  case Q of
    {error, _} ->
      Result = false,
      Ctx_Next = Ctx;
    _ ->
      Result = true,
      Ctx_Next = [Q | Ctx]
  end,
  {Result, RD, Ctx_Next}.

content_types_provided(RD, Ctx) ->
    {[{"application/json", to_json} ], RD, Ctx}.
 
to_json(RD, Ctx) ->
    % pop the Question
    [Q | Ctx_Next] = Ctx,
    Resp = q:to_json(Q),
    {Resp, RD, Ctx_Next}.
