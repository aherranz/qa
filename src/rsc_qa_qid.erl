-module(rsc_qa_qid).
 
-compile(export_all).
 
-include_lib("webmachine/include/webmachine.hrl").

-include("include/qa_records.hrl").
 
init(Config) ->
    {{trace, "priv/log"}, Config}. %% debugging
    % {ok, undefined}.

allowed_methods(RD, Ctx) ->
    {['GET', 'HEAD'], RD, Ctx}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GET
resource_exists(RD, Ctx) ->
    Qid = wrq:path_info(qid, RD),
    Q = q:read(Qid),
    case Q of
        {error, _} ->
            Result = false,
            Ctx_Next = Ctx;
        _ ->
            Result = true,
            % push the Question 
            Ctx_Next = [Q | Ctx]
    end,
    {Result, RD, Ctx_Next}.

content_types_provided(RD, Ctx) ->
    {[{"application/json", to_json} ], RD, Ctx}.
 
to_json(RD, Ctx) ->
    Qid = wrq:path_info(qid, RD),
    {q:to_json_array(a:all(Qid)), RD, Ctx}.
