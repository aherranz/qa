-module(q).

-compile(export_all).

-include("include/qa_records.hrl").

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CRUD API
create(Q) ->
    Qid = generate_id(),
    Question = Q#q{qid = Qid},
    update(Question).

read(Qid) when is_list(Qid)->
    read(list_to_integer(Qid));
read(Qid) ->
    Read = fun() -> mnesia:read(q, Qid) end,
    case mnesia:transaction(Read) of
        {atomic, [Q]} -> Q;
        {atomic, []} -> {error, not_exists}
    end.

update(Q) ->
    Write = fun() -> mnesia:write(Q) end,
    case mnesia:transaction(Write) of
        {atomic, ok} -> Q
    end.

delete(Qid) when is_list(Qid)->
    delete(list_to_integer(Qid));
delete(Qid) ->
    Delete = fun() -> mnesia:delete({a, Qid}) end,
    case mnesia:transaction(Delete) of
        {atomic, ok} -> ok
    end.

all() ->
    utils:search_pattern(#q{_ = '_'}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Representation
to_json(Q) ->
    mochijson:encode(to_json_struct(Q)).

to_json_struct(Q) ->
      {struct,[
               {qid, Q#q.qid},
               {statement, Q#q.statement}
              ]}.

to_json_array(L) ->
    mochijson:encode({array, lists:map(fun to_json_struct/1, L)}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helpers

%% TODO: use a more sophisticated id generator (https://github.com/travis/erlang-uuid)
generate_id() ->
    idgen:gen(q).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reset() ->
  mnesia:clear_table(idgen),
  mnesia:clear_table(a),
  mnesia:clear_table(q).

