-module(utils).

-compile(export_all).

% Transactional match_object in mnesia
search_pattern(Pattern) ->
    All = fun() -> mnesia:match_object(Pattern) end,
    case mnesia:transaction(All) of
        {atomic, Data} -> Data
    end.
