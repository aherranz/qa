-module(rsc_q).
 
-compile(export_all).
 
-include_lib("webmachine/include/webmachine.hrl").

-include("include/qa_records.hrl").
 
init(Config) ->
    %% {{trace, "priv/log"}, Config}. %% debugging
    {ok, undefined}.

allowed_methods(RD, Ctx) ->
    {['GET', 'POST', 'HEAD'], RD, Ctx}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GET
resource_exists(RD, Ctx) ->
    {true, RD, Ctx}.

content_types_provided(RD, Ctx) ->
    {[{"application/json", to_json} ], RD, Ctx}.
 
to_json(RD, Ctx) ->
    {q:to_json_array(q:all()), RD, Ctx}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% POST
% Although POST is create we don't know the path until the database
% generates the id so post_is_create returns false and create_path is
% not implemented.

%% We should probably verify that the content is json, but the documentation
%% on how to do that for webmachine is a bit confusing...
process_post(RD,Ctx) ->
    try mochijson:decode(wrq:req_body(RD)) of
	{struct,Data} ->
	Statement = proplists:get_value("statement",Data),
	Q_Data = #q{statement = Statement},
	Q = q:create(Q_Data),
	JSON = q:to_json(Q),
	Resp = wrq:set_resp_body(JSON,RD),
	{true,Resp,Ctx};

	%% Probably we should be careful what to return on failure...
	%% (something to test perhaps :-)
	_ -> {false,RD,Ctx}
    catch _:_ -> {false,RD,Ctx} end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helpers
