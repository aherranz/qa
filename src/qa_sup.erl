%% @author author <author@example.com>
%% @copyright YYYY author.

%% @doc Supervisor for the qa application.

-module(qa_sup).
-author('author <author@example.com>').

-behaviour(supervisor).

%% External exports
-export([start_link/0]).

%% supervisor callbacks
-export([init/1]).

%% @spec start_link() -> ServerRet
%% @doc API for starting the supervisor.
start_link() ->
    application:set_env(mnesia,dir,"qadb"),
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).


%% @spec init([]) -> SupervisorTree
%% @doc supervisor callback.
init([]) ->
  Web = {webmachine_mochiweb,
           {webmachine_mochiweb, start, [qa_config:web_config()]},
           permanent, 5000, worker, [mochiweb_socket_server]},
    Processes = [Web],
    {ok, { {one_for_one, 10, 10}, Processes} }.


