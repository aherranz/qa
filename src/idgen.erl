-module(idgen).

-export([gen/1]).

-include("include/qa_records.hrl").

%% TODO: use a more sophisticated id generator (https://github.com/travis/erlang-uuid)
gen(Table) -> mnesia:dirty_update_counter(idgen,Table,1).
