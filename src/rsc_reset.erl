-module(rsc_reset).
 
-compile(export_all).
 
-include_lib("webmachine/include/webmachine.hrl").

-include("include/qa_records.hrl").
 
init(Config) ->
    %% {{trace, "priv/log"}, Config}. %% debugging
    {ok, undefined}.

allowed_methods(RD, Ctx) ->
    {['POST'], RD, Ctx}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GET
resource_exists(RD, Ctx) ->
    {true, RD, Ctx}.

content_types_provided(RD, Ctx) ->
    {[{"application/json", to_json} ], RD, Ctx}.
 
to_json(RD, Ctx) ->
    {q:to_json_array(q:all()), RD, Ctx}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% POST
process_post(RD,Ctx) ->
    Body = wrq:req_body(RD),
    try mochijson:decode(Body) of
	{struct,Data} ->
	case proplists:get_value("reset",Data) of
	  true -> q:reset();
	  _ -> ok
	end,
	Resp = wrq:set_resp_body(Body,RD),
	{true,Resp,Ctx};

	%% Probably we should be careful what to return on failure...
	%% (something to test perhaps :-)
	_ -> {false,RD,Ctx}
    catch _:_ -> {false,RD,Ctx} end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helpers
