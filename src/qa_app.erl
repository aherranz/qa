%% @author author <author@example.com>
%% @copyright YYYY author.

%% @doc Callbacks for the qa application.

-module(qa_app).
-author('author <author@example.com>').

-behaviour(application).
-export([start/2,stop/1]).


%% @spec start(_Type, _StartArgs) -> ServerRet
%% @doc application start callback for qa.
start(_Type, _StartArgs) ->
    qa_sup:start_link().

%% @spec stop(_State) -> ServerRet
%% @doc application stop callback for qa.
stop(_State) ->
    ok.
