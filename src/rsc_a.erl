-module(rsc_a).
 
-compile(export_all).
 
-include_lib("webmachine/include/webmachine.hrl").

-include("include/qa_records.hrl").
 
init(Config) ->
    %%{{trace, "priv/log"}, Config}. %% debugging
    {ok, undefined}.

allowed_methods(RD, Ctx) ->
    {['GET', 'POST', 'HEAD'], RD, Ctx}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GET
resource_exists(RD, Ctx) ->
    {true, RD, Ctx}.

content_types_provided(RD, Ctx) ->
    {[{"application/json", to_json} ], RD, Ctx}.
 
to_json(RD, Ctx) ->
    {a:to_json_array(a:all()), RD, Ctx}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% POST
% Although POST is create we don't know the path until the database
% generates the id so post_is_create returns false and create_path is
% not implemented.

%% We should probably verify that the content is json, but the documentation
%% on how to do that for webmachine is a bit confusing...
process_post(RD,Ctx) ->
    try mochijson:decode(wrq:req_body(RD)) of
	{struct,Data} ->
	QID = proplists:get_value("qid",Data),
	UID = proplists:get_value("uid",Data),
	if
	  UID == undefined ->
	    JSON = {struct,[{error,"uid missing"}]},
	    Resp = wrq:set_resp_body(mochijson:encode(JSON),RD),
	    {{halt,409},Resp,Ctx};
	  true ->
	    Answer = proplists:get_value("answer",Data),
	    if
	      Answer == undefined ->
		JSON = {struct,[{error,"answer missing"}]},
		Resp = wrq:set_resp_body(mochijson:encode(JSON),RD),
		{{halt,409},Resp,Ctx};
	      true ->
		A_Data = #a{qid = QID, uid = UID, answer = Answer},
		case a:create_if_exists(A_Data) of
		  {ok,A} ->
		    JSON = a:to_json(A),
		    Resp = wrq:set_resp_body(JSON,RD),
		    {true,Resp,Ctx};
		  {Other,A} ->
		    io:format("it doesn't exist:~p:~p~n",[Other,A]),
		    {struct,PL} = a:to_json_struct(A),
		    io:format("p2~n",[]),
		    JSON =
		      {struct,
		       [{error,"question missing"}|PL]},
		    io:format("before encoding~n"),
		    Resp = wrq:set_resp_body(mochijson:encode(JSON),RD),
		    io:format("Encoded is ~s~n",[mochijson:encode(JSON)]),
		    io:format("Resp is ~p~n",[Resp]),
		    {{halt,409},Resp,Ctx}
		end
	    end
	end;

	%% Probably we should be careful what to return on failure...
	%% (something to test perhaps :-)
	_ -> {false,RD,Ctx}
    catch Class:Reason ->
	{false,RD,Ctx}
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helpers
