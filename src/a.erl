-module(a).

-compile(export_all).

-include("include/qa_records.hrl").

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CRUD API
create(A) ->
    Aid = generate_id(),
    Answer = A#a{aid = Aid},
    update(Answer).

read(Aid) when is_list(Aid)->
    read(list_to_integer(Aid));
read(Aid) ->
    Read = fun() -> mnesia:read(a, Aid) end,
    case mnesia:transaction(Read) of
        {atomic, [A]} -> A;
        {atomic, []} -> {error, not_exists}
    end.

update(A) ->
    Write = fun() -> mnesia:write(A) end,
    case mnesia:transaction(Write) of
        {atomic, ok} -> A
    end.

delete(Aid) when is_list(Aid)->
    delete(list_to_integer(Aid));
delete(Aid) ->
    Delete = fun() -> mnesia:delete({a, Aid}) end,
    case mnesia:transaction(Delete) of
        {atomic, ok} -> ok
    end.

all() ->
    utils:search_pattern(#a{_ = '_'}).

all(Qid) ->
    R = utils:search_pattern(#a{qid=Qid,  _ = '_'}),
    io:format("~p~n",[R]),
    R.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_if_exists(A) ->
  Aid = generate_id(),
  Answer = A#a{aid = Aid},
  Write =
    fun() ->
	[Q] = mnesia:read(q,A#a.qid),
	mnesia:write(Answer)
    end,
  case mnesia:transaction(Write) of
    {atomic, ok} -> {ok,Answer};
    Other -> {Other,Answer}
  end.
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Representation
to_json(A) ->
    mochijson:encode(to_json_struct(A)).

to_json_struct(A) ->
      {struct,[
               {aid, A#a.aid},
               {qid, A#a.qid},
               {uid, A#a.uid},
               {answer, A#a.answer}
              ]}.

to_json_array(L) ->
    mochijson:encode({array, lists:map(fun to_json_struct/1, L)}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helpers

%% TODO: use a more sophisticated id generator (https://github.com/travis/erlang-uuid)
generate_id() ->
    idgen:gen(a).
