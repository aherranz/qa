-module(wrq_xtra).

-compile(export_all).

%% Query params
req_qp(RD) ->
    mochiweb_util:parse_qs(wrq:req_body(RD)).

get_qp_value(Name, RD) ->
    proplists:get_value(Name,req_qp(RD)).
