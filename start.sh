#!/bin/sh
TO_ERL=/usr/lib/erlang/bin/to_erl
cd `dirname $0`
run_erl -daemon priv/log priv/log "exec erl -pa $PWD/_build/default/lib/*/ebin -boot start_sasl -s reloader -s qa -config etc/qawm.config"
echo "Run the following command to get attached to the QA Webmachine:"
echo ${TO_ERL} priv/log
exit 0
