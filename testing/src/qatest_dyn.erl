-module(qatest_dyn).

-compile(export_all).

-include_lib("eqc/include/eqc.hrl").
-include_lib("eqc/include/eqc_component.hrl").
-include_lib("eqc/include/eqc_dynamic_cluster.hrl").

-record(state,{qa,dirty}).

api_spec() ->
  #api_spec{}.

initial_state() ->
  spawn
    (fun () ->
	 case ets:info(qcdata) of
	   L when is_list(L) -> ok;
	   _ -> ets:new(qcdata,[named_table,public]), wait_forever()
	 end
     end),
  wait_until_stable(),
  genput(q_gen,jsg("src/question.jsch")),
  genput(a_gen,jsg("src/answer.jsch")),
  genget(a_gen),
  genget(q_gen),
  #state{qa=[],dirty=true}.

wait_until_stable() ->
  case ets:info(qcdata) of
    L when is_list(L) ->
      ok;
    _ ->
      wait_until_stable()
  end.

wait_forever() ->
  receive _ -> wait_forever() end.

reset_pre(State) ->
  State#state.dirty.

reset_args(_) ->
  [].

reset_post(_State,_,Result) ->
  case analyze_http_result(Result) of
    {normal,{200,Body}} -> check_if_json_body(Body);
    _ -> false
  end.

reset_next(State,_,_) ->
  State#state{dirty=false}.

reset() ->
  post_to_rest("reset",{struct,[{<<"reset">>,true}]}).

add_question_pre(State) ->
  not(State#state.dirty).

add_question_args(State) ->
  [genget(q_gen)].

add_question_post(_State,[_Body],Result) ->
  %%io:format("post_question: state is ~p~n",[_State#state.qa]),
  case analyze_http_result(Result) of
    {normal,{200,ResultBody}} ->
      %%io:format("body is ~p~n",[ResultBody]),
      json_body_has_property(ResultBody,<<"qid">>);
    _ -> false
  end.

add_question_next(State,V,[_Body]) ->
  NewQA = add_question_to_qa(V,State#state.qa),
  State#state{qa=NewQA}.

add_question_to_qa(V,QA) ->
  try add_question_to_qa1(V,QA)
  catch Class:Reason ->
      %%io:format
	%%("exception ~p:~p in add_question_to_qa~nStacktrace:~n~p~n",
	 %%[Class,Reason,erlang:get_stacktrace()]),
      erlang:raise(Class,Reason)
  end.

add_question_to_qa1(V,QA) ->
  %%io:format("add_question_to_qa(~p,~p)~n",[V,QA]),
  {ok,JSON} = to_json(body(V)),
  {value,Qid} = get_json_property(JSON,<<"qid">>),
  {value,Statement} = get_json_property(JSON,<<"statement">>),
  lists:keystore(Qid,1,QA,{Qid,{Statement,[]}}).

add_question(Body) ->
  post_to_rest("q",Body).

add_answer_pre(State) ->
  not(State#state.dirty).

add_answer_args(State) ->
  [genget(a_gen)].

add_answer(Body) ->
  post_to_rest("a",Body).

add_answer_post(State,[Body],Result) ->
  case get_json_property(Body,<<"qid">>) of
    {value,Qid} ->
      ShouldSucceed = (lists:keyfind(Qid,1,State#state.qa)=/=false),
      AResult = analyze_http_result(Result),
      io:format("ShouldSucceed=~p Result=~p~n",[ShouldSucceed,AResult]),
      case AResult of
	{normal,{Code,ResultBody}} ->
	  if
	    ShouldSucceed, Code==200 ->
	      io:format("has_property aid?~n"),
	      json_body_has_property(ResultBody,<<"aid">>);
	    not(ShouldSucceed), Code==409 ->
	      io:format("has_property error?~n"),
	      json_body_has_property(ResultBody,<<"error">>);
	    true ->
	      io:format
		("Error: ShouldSucceed=~p Code=~p~n",
		 [ShouldSucceed,Code]),
	      false
	  end;
	_ ->
	  io:format("Result is badly formed: ~p~n",[AResult]),
	  false
      end
  end.

add_answer_next(State,V,[_Body]) ->
  NewQA = add_answer_to_qa(V,State#state.qa),
  State#state{qa=NewQA}.

add_answer_to_qa(V,QA) ->
  try add_answer_to_qa1(V,QA)
  catch Class:Reason ->
      io:format
	("exception ~p:~p in add_answer_to_qa~nStacktrace:~n~p~n",
	 [Class,Reason,erlang:get_stacktrace()]),
      erlang:raise(Class,Reason)
  end.

add_answer_to_qa1(V,QA) ->
  io:format("add_answer_to_qa(~p,~p)~n",[V,QA]),
  {ok,JSON} = to_json(body(V)),
  case get_json_property(JSON,<<"error">>) of
    {value,_} -> QA;
    _ ->
      {value,Qid} = get_json_property(JSON,<<"qid">>),
      {value,Answer} = get_json_property(JSON,<<"answer">>),
      {value,Aid} = get_json_property(JSON,<<"aid">>),
      Reply = {Aid,Answer},
      case lists:keyfind(Qid,1,QA) of
	false -> 
	  QA;
	{Text,Answers} ->
	  lists:keyreplace(Qid,1,QA,{Qid,{Text,[Reply|Answers]}})
      end
  end.

post_to_rest(Suffix,Body) ->
  JSON = mochijson2:encode(Body),
  io:format("will post ~s to ~p~n",[JSON,Suffix]),
  Response =
    httpc:request
    (post,
     {"http://127.0.0.1:8000/"++Suffix,
      [],
      "application/json",
      iolist_to_binary(JSON)},
     [{timeout,1500}],
     []),
  io:format("Response was ~p~n",[Response]),
  Response.

jsg(FileName) ->
  io:format("jsg(~s)~n",[FileName]),
  {ok,Schema} = jsonschema:read_schema(FileName),
  jsongen:json(Schema).

body(Result) ->
  {ok,{_StatusLine,_Headers,RespBody}} = Result,
  RespBody.

analyze_http_result(Result) ->  
  case Result of 
    {ok,{StatusLine,_Headers,RespBody}} ->
      case StatusLine of
	{_,ResponseCode,_} ->
	  {normal,{ResponseCode,RespBody}};
	_ ->
	  {other,Result}
      end;
    _ -> 
      {other,Result}
  end.

get_json_property(JBody,Property) ->
  case to_json(JBody) of
    {ok,{struct,PropList}} ->
      case proplists:get_value(Property,PropList) of
	undefined -> false;
	Value -> {value,Value}
      end;
    _ -> false
  end.

check_if_json_body(JBody) ->
  case to_json(JBody) of
    {ok,{struct,_}} -> true;
    _ -> false
  end.

json_body_has_property(JBody,Property) -> 
  case to_json(JBody) of
    {ok,{struct,PropList}} -> 
      case proplists:get_value(Property,PropList) of
	undefined -> false;
	_ -> true
      end;
    _ -> false
  end.

to_json(L) when is_list(L) -> 
  try mochijson2:decode(L) of
      Value -> {ok,Value}
  catch _:_ -> error end;
to_json(Other) ->
  {ok,Other}.

prop_ok() ->
  ?FORALL(Cmds, eqc_dynamic_cluster:dynamic_commands(?MODULE),
	  ?CHECK_COMMANDS({H, DS, Res}, ?MODULE, Cmds,
	  begin
	    pretty_commands(?MODULE, Cmds, {H, DS, Res},
			    Res == ok)
	  end)).

test() ->
  case eqc:quickcheck(prop_ok()) of
    false ->
      io:format
	("~n~n***FAILED; counterexample is~n~n~p~n",
	 [eqc:counterexample()]);
    true ->
      io:format
	("~n~nPASSED~n",[])
  end.
      
genput(Gen,Data) ->
  ets:insert(qcdata,{{gen,Gen},Data}).

genget(Gen) ->
  case ets:lookup(qcdata,{gen,Gen}) of
    [{_,Data}] ->
      Data;
    undefined ->
      io:format("*** Error: generator ~p is undefined~n",[Gen]),
      throw(bad)
  end.
